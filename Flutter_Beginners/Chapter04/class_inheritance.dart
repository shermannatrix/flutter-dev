enum PersonType { student, employee }

class Person {
	late String firstName;
	late String lastName;
	PersonType? type;

	Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}


	String get fullName => "$firstName $lastName";
}

class Student extends Person {
	String nickName;
	Student(String firstName, String lastName, this.nickName) 
		: super(firstName, lastName);
	
	@override
	String toString() => "$fullName, aka $nickName";
}

void main() {
	print(PersonType.values);
	Person somePerson = Person("Clark", "Kent");
	somePerson.type = PersonType.employee;
	print(somePerson.type);
	print(somePerson.type?.index);
	print(describeEnum(PersonType.employee));
}