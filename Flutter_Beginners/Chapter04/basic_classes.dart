+class Person {
	late String firstName;
	late String lastName;

	static String personLabel = "Person name:";

	String get fullName => "$personLabel $firstName $lastName";

	set fullName(String fullName) {
		var parts = fullName.split(" ");
		this.firstName = parts.first;
		this.lastName = parts.last;
	}

	String get initials => "${firstName[0]}. ${lastName[0]}.";
}

void main() {
	Person somePerson = Person();
	somePerson.firstName = "Clark";
	somePerson.lastName = "Kent";
	Person anotherPerson = Person();
	anotherPerson.firstName = "Peter";
	anotherPerson.lastName = "Parker";
	print(somePerson.fullName);
	print(anotherPerson.fullName);
	Person.personLabel = "name:";
	print(somePerson.fullName);
	print(anotherPerson.fullName);
}