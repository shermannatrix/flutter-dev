void main() {
  List dynamicList = [];
  print(dynamicList.length);
  dynamicList.add("Hello");
  print(dynamicList[0]);
  print(dynamicList.length);
  List preFilledDynamicList = [1, 2, 3];
  print(preFilledDynamicList[0]);
  print(preFilledDynamicList.length);

  List fixedList = List.filled(3, "World");
  //fixedList.add("Hello");
  fixedList[0] = "Hello";
  print(fixedList[0]);
  print(fixedList[1]);
}
