String sayHappyBirthday(String name, [int age = 21]) {
	return "Happy Birthday $name! You are $age years old.";
}

void main() {
	var birthdayGreeting = sayHappyBirthday('Robert');
	print(birthdayGreeting);
}