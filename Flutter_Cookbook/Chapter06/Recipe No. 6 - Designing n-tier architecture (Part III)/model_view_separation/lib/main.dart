import 'package:flutter/material.dart';
import 'package:model_view_separation/models/data_layer.dart';
import 'package:model_view_separation/plan_provider.dart';
import 'package:model_view_separation/views/plan_creator_screen.dart';

void main() => runApp(PlanProvider(child: MasterPlanApp()));