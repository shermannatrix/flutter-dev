import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:stopwatch/platform_alert.dart';

class StopWatch extends StatefulWidget {
	static const route = '/stopwatch';
	late String? name;
	late String? email;

	StopWatch({
		Key? key, 
		this.name,
		this.email }) : super(key: key);

	@override
	State createState() => StopWatchState();
}

class StopWatchState extends State<StopWatch> {
	int milliseconds = 0;
	final laps = <int>[];
	late Timer timer;
	bool isTicking = true;

	final itemHeight = 60.0;
	final scrollController = ScrollController();

	void _onTick(Timer time) {
		setState(() {
			milliseconds += 100;
		});
	}

	@override
	Widget build(BuildContext context) {
		String name = ModalRoute.of(context)!.settings.arguments as String ?? '';

		return Scaffold(
			appBar: AppBar(
				title: Text(name),
			),
			body: Column(
				children: <Widget>[
					Expanded(child: _buildCounter(context)),
					Expanded(child: _buildLapDisplay()),
				],
			),
		);
	}

	Widget _buildCounter(BuildContext context) {
		return Container(
			color: Theme.of(context).primaryColor,
			child: Column(
				mainAxisAlignment: MainAxisAlignment.center,
				children: <Widget>[
					Text(
						'Lap ${laps.length + 1}',
						style: Theme.of(context).textTheme.subtitle1?.copyWith(color: Colors.white),
					),
					Text(
						_secondsText(milliseconds),
						style: Theme.of(context).textTheme.headline5?.copyWith(color: Colors.white),
					),
					SizedBox(height: 20),
					_buildControls(),
				],
			),
		);
	}

	Widget _buildControls() {
		return Row(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						ElevatedButton(
							style: ButtonStyle(
								backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
								foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
							),
							child: Text('Start'),
							onPressed: isTicking ? _startTimer : null,
						),
						SizedBox(width: 20),
						ElevatedButton(
							style: ButtonStyle(
								backgroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
							),
							child: Text('Lap'),
							onPressed: isTicking ? _lap : null,
						),
						SizedBox(width: 20),
						Builder(
							builder: (context) => TextButton (
								style: ButtonStyle(
									backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
									foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
								),
								child: Text('Stop'),
								onPressed: isTicking ? () => _stopTimer(context) : null,
							),
						),
					],
				);
	}

	Widget _buildLapDisplay() {
		return Scrollbar(
			child: ListView.builder(
				controller: scrollController,
				itemExtent: itemHeight,   // Fixed height for each ListTile item
				itemCount: laps.length,
				itemBuilder: (context, index) {
					final milliseconds = laps[index];
					return ListTile(
						contentPadding: EdgeInsets.symmetric(horizontal: 50),
						title: Text('Lap ${index + 1}'),
						trailing: Text(_secondsText(milliseconds)),
					);
				},
			)
		);
	}

	void _startTimer() {
		timer = Timer.periodic(Duration(milliseconds: 100), _onTick);

		setState(() {
			milliseconds = 0;
			laps.clear();
			isTicking = true;
		});
	}

	void _stopTimer(BuildContext context) {
		timer.cancel();
		setState(() {
			isTicking = false;
		});

		final controller = showBottomSheet(context: context, builder: _buildRunCompleteSheet);

		Future.delayed(Duration(seconds: 5)).then((_) {
			controller.close();
		});
	}

	Widget _buildRunCompleteSheet(BuildContext context) {
		int totalRuntime = laps.fold(milliseconds, (total, lap) => total + lap);
		final textTheme = Theme.of(context).textTheme;

		return SafeArea(
			child: Container(
				color: Theme.of(context).cardColor,
				width: double.infinity,
				child: Padding(
					padding: const EdgeInsets.symmetric(vertical: 30.0),
					child: Column(
						mainAxisSize: MainAxisSize.min,
						children: [
							Text('Run Finished!', style: textTheme.headline6),
							Text('Total Run Time is ${_secondsText(totalRuntime)}.')
						],
					),
				),
			),
		);
	}

	String _secondsText(int milliseconds) {
		final seconds = milliseconds / 1000;
		return '$seconds seconds';
	}

	void _lap() {
		setState(() {
			laps.add(milliseconds);
			milliseconds = 0;
		});

		scrollController.animateTo(
			itemHeight * laps.length,
			duration: Duration(milliseconds: 500),
			curve: Curves.easeIn,);
	}

	@override
	void dispose() {
		timer.cancel();
		super.dispose();
	}
}
